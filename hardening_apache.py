#!/usr/bin/python
# -*- coding: utf-8 -*-

#30/11/2016
#Pedro Al PAcheco - pedro.pacheco.a@gmail.com - papacheco@furb.br 
#Purpuse of this script is to check some basic apache security itens.

import os
import commands
#import subprocess

#Variables+++++++++

#Check wich apache directory debian or red hat
#Check to see if it is debian or red hat
dirdebian = "/etc/apache2"
dirredhat = "/etc/httpd"

#buscaum = "Indexes"
buscaum = "Options"
buscadois = "ServerTokens"
buscatres = "ServerSignature"
buscaquatro = "FileETag"
buscacinco = "TraceEnable"
buscaseis = "Includes"
buscasete = "Header set X-XSS-Protection"
buscaoito = "RewriteEngine"
buscanove = "Timeout "
buscadez = "SSLProtocol"
buscaonze = "HostnameLookups"

#Functions=========

def ver_user_apache():
	"Check which user or apache is running"
	if existe == False:
		servico = "apache2"
	else:
		servico = "http"
	user = commands.getoutput("ps aux | grep "+servico)
	print(user)
	#print(servico)


def lista_modulo():
	"List the active modules in apache"
	#os.system("apachectl -M")
	modulos = commands.getoutput("apachectl -M")
	print (modulos)

def busca_var(diretorioapache,var_busca):
	"This is the function of searching for variables within directories"
	os.chdir(diretorioapache)
	leitura = commands.getoutput("grep -iR "+var_busca)
	print(leitura)

def fim_relatorio():
	"End the report"
	print("	")
	print("===================>FIM")
	print("	")

def espaco():
	print("	")

###End of duties============

existe = os.path.isdir(dirredhat)
if existe == False:
	print("SO debian")
	print("**Verifica Listagem de diretórios**Ideal é habilitado -Index")
	print("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#options")
	espaco()
	busca_var(dirdebian,buscaum)
	fim_relatorio()
	print("**Verifica os tipos de informação do servidor**Ideal é habilitado ServerTokens Prod")
	print ("Referencia:http://httpd.apache.org/docs/2.4/mod/core.html#servertokens")
	espaco()
	busca_var(dirdebian,buscadois)
	fim_relatorio()
	print("**Verifica a assinatura do apache**Ideal é habilitado ServerSignature Off")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#serversignature")
	espaco()
	busca_var(dirdebian,buscatres)
	fim_relatorio()
	print("**Verifica FileETag do apache**Ideal é habilitado FileETag None")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#fileetag")
	espaco()
	busca_var(dirdebian,buscaquatro)
	fim_relatorio()
	print("**Verifica TraceEnable do apache**Ideal é TraceEnable off")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#traceenable")
	espaco()
	busca_var(dirdebian,buscacinco)
	fim_relatorio()
	print("**Verifica os modulos ativos do apache")
	espaco()
	lista_modulo()
	fim_relatorio()
	print("**Verifica o usario do apache, ideal que não esteja como root")
	espaco()
	ver_user_apache()
	fim_relatorio()
	print("**Verifica conteudo dinamico no apache, ideal que esteja como -Includes")
	print ("https://httpd.apache.org/docs/current/howto/ssi.html")
	espaco()
	busca_var(dirdebian,buscaseis)
	fim_relatorio()
	print("**Verifica cross-site scripting no apache, O ideal é que tenha")
	print ("https://kb.sucuri.net/warnings/hardening/headers-x-xss-protection")
	#https://kb.sucuri.net/warnings/hardening/headers-x-xss-protection
	#Install module:a2enmod headers
	espaco()
	busca_var(dirdebian,buscasete)
	fim_relatorio()
	print("**Verifica desabilitar protocolo HTTP 1.0 no apache")
	print ("https://httpd.apache.org/docs/current/howto/ssi.html")
	#a2enmod rewrite
	#Enabling module rewrite
	espaco()
	busca_var(dirdebian,buscaoito)
	fim_relatorio()
	print("**Verifica Timeout no apache")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#timeout")
	espaco()
	busca_var(dirdebian,buscanove)
	fim_relatorio()
	print("**Verifica SSL V2 e V3,bloquear posteriores, ideal adicionar SSLProtocol -ALL +TLSv1 +TLSv1.1 +TLSv1.2")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#timeout")
	#Required apt-get install sslscan
	#exemple checks scan: sslscan –no-failed www.localhost
	#https://geekflare.com/ssl-test-certificate/
	espaco()
	busca_var(dirdebian,buscadez)
	fim_relatorio()
	print("**Verifica HostnameLookups no apache, o ideal é HostnameLookups off")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#hostnamelookups")
	espaco()
	busca_var(dirdebian,buscaonze)
	fim_relatorio()
else:
	print("SO Red Hat")
	print("**Verifica Listagem de diretórios**Ideal é habilitado -Index")
	print("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#options")
	espaco()
	busca_var(dirredhat,buscaum)
	fim_relatorio()
	print("**Verifica os tipos de informação do servidor**Ideal é habilitado ServerTokens Prod")
	print ("Referencia:http://httpd.apache.org/docs/2.4/mod/core.html#servertokens")
	espaco()
	busca_var(dirredhat,buscadois)
	fim_relatorio()
	print("**Verifica a assinatura do apache**Ideal é habilitado ServerSignature Off")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#serversignature")
	espaco()
	busca_var(dirredhat,buscatres)
	fim_relatorio()
	print("**Verifica FileETag do apache**Ideal é habilitado FileETag None")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#fileetag")
	espaco()
	busca_var(dirredhat,buscaquatro)
	fim_relatorio()
	print("**Verifica TraceEnable do apache**Ideal é TraceEnable off")
	print ("Referencia:https://httpd.apache.org/docs/2.4/mod/core.html#traceenable")
	espaco()
	busca_var(dirredhat,buscacinco)
	fim_relatorio()
	print("**Verifica os modulos ativos do apache")
	espaco()
	lista_modulo()
	fim_relatorio()
	print("**Verifica o usario do apache, ideal que não esteja como root")
	espaco()
	ver_user_apache()
	fim_relatorio()
	print("**Verifica conteudo dinamico no apache, ideal que esteja como -Includes")
	print ("https://httpd.apache.org/docs/current/howto/ssi.html")
	espaco()
	busca_var(dirredhat,buscaseis)
	fim_relatorio()
	print("**Verifica cross-site scripting no apache, O ideal é que tenha")
	print ("https://kb.sucuri.net/warnings/hardening/headers-x-xss-protection")
	#https://kb.sucuri.net/warnings/hardening/headers-x-xss-protection
	#Install module:a2enmod headers
	espaco()
	busca_var(dirredhat,buscasete)
	fim_relatorio()
	print("**Verifica desabilitar protocolo HTTP 1.0 no apache")
	print ("https://httpd.apache.org/docs/current/howto/ssi.html")
	#a2enmod rewrite
	#Enabling module rewrite
	espaco()
	busca_var(dirredhat,buscaoito)
	fim_relatorio()
	print("**Verifica Timeout no apache")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#timeout")
	espaco()
	busca_var(dirredhat,buscanove)
	fim_relatorio()
	print("**Verifica SSL V2 e V3,bloquear posteriores, ideal adicionar SSLProtocol -ALL +TLSv1 +TLSv1.1 +TLSv1.2")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#timeout")
	#Required apt-get install sslscan
	#exemple checks scan: sslscan –no-failed www.localhost
	#https://geekflare.com/ssl-test-certificate/
	espaco()
	busca_var(dirredhat,buscadez)
	fim_relatorio()
	print("**Verifica HostnameLookups no apache, o ideal é HostnameLookups off")
	print ("https://httpd.apache.org/docs/2.4/mod/core.html#hostnamelookups")
	espaco()
	busca_var(dirredhat,buscaonze)
	fim_relatorio()
